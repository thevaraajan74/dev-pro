const provider = require("../models/provider.model");
const { getlonglive } = require("../library/facebook");
// single provider getbyid service
exports.providerDetails = async (req, res) => {
  const Findprovider = await provider.findOne({ userId: req.query.userId });

  if (!Findprovider)
    return res.status(400).send({ status: 0, message: "Invalid provider" });

  return res.status(200).send({
    status: 1,
    message: "provider Details retrieved successfully.",
    data: Findprovider,
  });
};
// update provider service
exports.Updateprovider = async (req, res) => {
  const UpUser = await provider.findOneAndUpdate(
    { userId: req.params.userId },
    req.body,
    { new: true }
  );

  return res.status(200).send({
    status: 1,
    message: "provider updated successfully.",
    data: UpUser,
  });
};
//delete provider service
exports.Deleteprovider = async (req, res) => {
  const UpUser = await provider
    .findOneAndRemove({ userId: req.query.userId }, { new: true })

    .then((datas) => {
      return res
        .status(200)
        .send({ status: 1, message: "provider  Deleted successfully." });
    })
    .catch((error) => {
      return res.status(400).send({
        status: 0,
        message:
          error.code === 11000
            ? Object.keys(error.keyValue)[0] + " is already exists"
            : error.message,
      });
    });
};
// new provider create  service
exports.Createprovider = async (req, res) => {
    const Createban = await new provider(req.body);
    Createban.save();
    if(Createban){

      var access = await getlonglive(Createban);     
      var data1 = await provider.findOne({_id:Createban._id}) 
      let data=data1.response[0]        
      var results = await provider.findOneAndUpdate({_id:Createban._id},{response:{accessToken:access,userID:data.userID,expiresIn:data.expiresIn,signedRequest:data.signedRequest,graphDomain:data.graphDomain,data_access_expiration_time:data.data_access_expiration_time}});      
      return res.status(200).send({ status: 0, message: "Provider Successfully created", results });
    }
    return res
    .status(500)
    .send({ status: 0, message: "something went wrong" });
  }

//provider getall service
exports.getallprovider = async (req, res) => {
  var filter = {};

  if (
    req.query.is_active &&
    req.query.is_active != undefined &&
    req.query.is_active != null
  )
    filter["is_active"] = (req.query.is_active = true) ? true : false;

  const Findprovider = await provider.find(filter);

  return res.status(200).send({
    status: 1,
    message: "All provider Details retrieved successfully.",
    data: Findprovider,
  });
};
