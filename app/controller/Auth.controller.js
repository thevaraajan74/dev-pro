const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.LoginUser = async (req, res) => {
  if (!req.body.email || !req.body.password)
    return res
      .status(400)
      .send({ status: 0, message: "Required field can`t empty" });

  const FindUser = await User.findOne({
    email: req.body.email,
    is_deleted: false,
  });

  if (!FindUser)
    return res
      .status(400)
      .send({ status: 0, message: "Invalid Username/Password" });

  if (FindUser.is_verified === false)
    return res
      .status(400)
      .send({ status: 0, message: "Please verify your account" });

  if (FindUser.is_active === false)
    return res.status(400).send({ status: 0, message: "Please contact Admin" });

  const isMatch = await bcrypt.compare(req.body.password, FindUser.password);

  if (isMatch === false)
    return res
      .status(400)
      .send({ status: 0, message: "Invalid Username/Password" });

  const payload = { _id: FindUser._id };

  const Token = jwt.sign(payload, process.env.PRIVATE_KEY, {
    expiresIn: process.env.USER_TIME,
  });

  // Update Token to User schema

  const UpdateUser = await User.findByIdAndUpdate(
    FindUser._id,
    { token: Token },
    { new: true }
  );

  if (!UpdateUser)
    return res.status(400).send({ status: 0, message: "token cant store " });

  return res
    .status(200)
    .send({ status: 1, message: "Login Successfully", data: UpdateUser });
};

exports.LogoutUser = async (req, res) => {
  if (!req.body.token)
    return res
      .status(400)
      .send({ status: 0, message: "Required field can`t empty" });

  const FindUser = await User.findOne({ token: req.body.token });

  if (!FindUser)
    return res.status(400).send({ status: 0, message: "Invalid Token" });

  // Update Token to User schema

  const UpUser = await User.findByIdAndUpdate(
    FindUser._id,
    { token: null },
    { new: true }
  );

  return res.status(200).send({ status: 1, message: "Logout Successfully" });
};

exports.changepasssword = async (req, res) => {
  debugger;

  if (
    !req.body.userId &&
    !req.body.current &&
    !req.body.new_password &&
    !req.body.confirm_password
  )
    return res
      .status(400)
      .send({ status: 0, message: "Required field can`t empty" });

  const FindUser = await User.findOne({
    _id: req.body.userId,
    is_active: true,
    is_deleted: false,
    is_verified: true,
  });

  bcrypt.compare(req.body.current, FindUser.password, async (err, result) => {
    if (!result)
      return res.status(500).send({ message: "old password invalid", err });

    if (req.body.new_password !== req.body.confirm_password)
      return res.status(400).send({
        status: 0,
        message: "newPassword/confirm password  doesn`t match",
      });

    req.body.new_password = bcrypt.hashSync(req.body.new_password, 10);

    const UpdatedUser = await User.findByIdAndUpdate(
      FindUser._id,
      { password: req.body.new_password },
      { new: true }
    );

    if (!UpdatedUser)
      return res.status(400).send({ status: 0, message: "Server Error" });

    return res.status(200).send({
      status: 0,
      message: "Password reset Successully, Please Login.",
    });
  });
};
exports.Tokenverfiy = async (req, res) => {
  var Token = req.body.Token;
  if (Token) {
    try {
      let id = jwt.verify(Token, process.env.PRIVATE_KEY);

      const verify = await User.findById(id._id);
      if (Token == verify.token) {
        return res
          .status(200)
          .send({ message: "Success", data: { isValid: true } });
      }
    } catch (err) {
      return res
        .status(200)
        .send({ message: "Success", data: { isValid: false } });
    }
  }
};
