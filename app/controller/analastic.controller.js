const {
  getdata2,
  getdata,
  getadaccount,
  getbrackdown,
  getcampaign,
  getpage,
  getpageid,
  getFbUserDetails,
  pageToken,
} = require("../library/facebook");
const {
  instaBussinessaccount,
  getpages,
  follwercount,
} = require("../library/instagram");
const moment = require("moment");
const provider = require("../models/provider.model");
const User = require("../models/user.model");

exports.analyticscountry = async (req, res) => { 
 
  if (
    (!req.body.adAccount || !req.body.campaignId) &&
    !req.body.userId &&
    (!req.body.dateFilter || !req.body.fromDate || !req.body.toDate)
  )
    return res.status(400).send({ message: "Check your input params" });

  try {
    var userResponse = await User.findById(req.body.userId);

    if (!userResponse)
      return res.status(404).send({ message: "User Not found" });

    const businessResponse = await provider.findOne({
      userId: req.body.userId,
    });

    if (!businessResponse)
      return res.status(404).send({ message: " userID not found" });

    if (
      req.body.dateFilter == "thisMonth" ||
      req.body.dateFilter == "Last7Days" ||
      req.body.fromDate ||
      req.body.toDate
    ) {
      req.body.date =2;
    } else {
      req.body.date = "monthly";
    }
    var weekdays = [];

    var weekdaysconverted = [];

    if (req.body.dateFilter == "Last7Days") {
      weekdays = [];
      for (var i = 0; i < 7; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "thisMonth") {
      weekdays = [];
      var sDate = moment().startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM-DD"));
        weekdaysconverted.push(tDate.format("MMM-DD"));
        tDate = moment(tDate).subtract(1, "days");
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "sixMonth") {
      var sDate = moment().subtract(5, "months").startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        tDate = moment(tDate).subtract(1, "month");
        weekdaysconverted.push(tDate.format("MMMM"));
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "thisYear") {
      weekdays = [];
      var sDate = moment().startOf("year");
      var eDate = moment().endOf("year");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        weekdaysconverted.push(tDate.format("MMMM"));
        tDate = moment(tDate).subtract(1, "month");
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "allYear") {
      weekdays = [];
      for (var i = 0; i < 3; i++) {
        weekdays.push(moment().subtract(i, "year").format("YYYY"));
        weekdaysconverted.push(moment().subtract(i, "year").format("YYYY"));
      }
      req.body.fromRange = weekdays[0].concat("-12-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01-01");
    } else if (req.body.fromDate && req.body.toDate) {
      req.body.toDate = moment(req.body.toDate).add(1, "days");
      weekdays = [];
      for (
        var m = moment(req.body.fromDate);
        m.isBefore(req.body.toDate);
        m.add(1, "days")
      ) {
        weekdays.push(m.format("YYYY-MM-DD"));
        weekdaysconverted.push(m.format("YYYY-MMM-DD"));
      }
      req.body.toRange = weekdays[0];
      req.body.fromRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Last30Days") {
      weekdays = [];
      for (var i = 0; i < 30; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Yesterday") {
      weekdays = [];
      weekdays.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Today") {
      weekdays = [];
      weekdays.push(moment().subtract(0, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    }
    req.body.fields = 'country';
    // req.body.field ="publisher_platform";

    var result = await getbrackdown(req.body);
          
    
      if (req.body.campaignId) {
        let a = result.data.filter(j=> {
          // var cost = j.campaign_id;
          return j.campaign_id == req.body.campaignId;
        });
        result.data =[...a]
      }
     
    var allDataWithDate = [];
    let sortWeekdays = weekdays.sort();

    for (let x of sortWeekdays) {
      for (let y of result.data) {
       
        if (
          req.body.dateFilter == "sixMonth" ||
          req.body.dateFilter == "thisYear"
        ) {
          if (y.date_start.includes(x) == true) {
            allDataWithDate.push(y);
          }
        } else if (req.body.dateFilter == "allYear") {
          if (y.date_start.includes(x)) {
            allDataWithDate.push(y);
          }
        } else if (
          req.body.dateFilter == "Today" ||
          req.body.dateFilter == "Yesterday"
        ) {
          if (y.date_start.includes(x)) {
            allDataWithDate.push(y);
          }
        } else {
          if (y.date_start == x) {
            allDataWithDate.push(y);
          }
        }
      }
    }
    let countrieslist =[]
  if (allDataWithDate.length >= 1 ) { 
      let countries = allDataWithDate.reduce((add, item) => {
        if (!add[item.country]) {
          add[item.country] = [];
        }
        add[item.country].push(item);
        return add;
      }, {});
      /* Combining into single array */

      countrieslist = Object.values(countries);

      countrieslist = countrieslist.map((t) => {
        if (t.length > 1) {
          t = t.reduce((previousValue, currentValue) => {
            return {
              reach:
                parseInt(previousValue.reach) + parseInt(currentValue.reach),
              country: currentValue.country,
            };
          });
        } else {
          t = Object.assign({
            reach: t[0].reach,
            country: t[0].country,
          });
        }
        return t;
      });
    }
    if (!result.data)
      return res.status(500).send({ message: "Something went wrong" });

    return res.status(200).send({ message: "Success", data: countrieslist });
  } catch (e) {
    return res
      .status(500)
      .send({ message: "Something went wrong!", data: e.message });
  }
};
exports.analyticsage = async (req, res) => {  

  if (
    (!req.body.adAccount || !req.body.campaignId) &&
    !req.body.userId &&
    (!req.body.dateFilter || !req.body.fromDate || !req.body.toDate)
  ) {
    return res.status(400).send({ message: "Check your input params" });
  }
  try {
    var userResponse = await User.findById(req.body.userId);

    if (!userResponse)
      return res.status(404).send({ message: "User Not found" });

    const businessResponse = await provider.findOne( {userId: req.body.userId});


    if (!businessResponse)
      return res.status(404).send({ message: " userID not found" });

    if (
      req.body.dateFilter == "thisMonth" ||
      req.body.dateFilter == "Last7Days" ||
      req.body.fromDate ||
      req.body.toDate
    ) {
      req.body.date = 2;
    } else {
      req.body.date = "monthly";
    }
    var weekdays = [];
    var weekdaysconverted = [];
    if (req.body.dateFilter == "Last7Days") {
      weekdays = [];
      for (var i = 0; i < 7; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "thisMonth") {
      weekdays = [];
      var sDate = moment().startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM-DD"));
        weekdaysconverted.push(tDate.format("MMM-DD"));
        tDate = moment(tDate).subtract(1, "days");
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "sixMonth") {
      var sDate = moment().subtract(5, "months").startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        tDate = moment(tDate).subtract(1, "month");
        weekdaysconverted.push(tDate.format("MMMM"));
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "thisYear") {
      weekdays = [];
      var sDate = moment().startOf("year");
      var eDate = moment().endOf("year");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        weekdaysconverted.push(tDate.format("MMMM"));
        tDate = moment(tDate).subtract(1, "month");
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "allYear") {
      weekdays = [];
      for (var i = 0; i < 3; i++) {
        weekdays.push(moment().subtract(i, "year").format("YYYY"));
        weekdaysconverted.push(moment().subtract(i, "year").format("YYYY"));
      }
      req.body.fromRange = weekdays[0].concat("-12-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01-01");
    } else if (req.body.fromDate && req.body.toDate) {
      req.body.toDate = moment(req.body.toDate).add(1, "days");
      weekdays = [];
      for (
        var m = moment(req.body.fromDate);
        m.isBefore(req.body.toDate);
        m.add(1, "days")
      ) {
        weekdays.push(m.format("YYYY-MM-DD"));
        weekdaysconverted.push(m.format("YYYY-MMM-DD"));
      }
      req.body.toRange = weekdays[0];
      req.body.fromRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Last30Days") {
      weekdays = [];
      for (var i = 0; i < 30; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Yesterday") {
      weekdays = [];
      weekdays.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Today") {
      weekdays = [];
      weekdays.push(moment().subtract(0, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    }
    let genderValue = [
      {
        name: "male",
        data: [],
      },
      {
        name: "female",
        data: [],
      },
      {
        name: "unknown",
        data: [],
      },
    ];
    let ageValue = [
      {
        name: "13-17 Yrs",
        data: [],
      },
      {
        name: "18-24 Yrs",
        data: [],
      },
      {
        name: "25-34 Yrs",
        data: [],
      },
      {
        name: "34-44 Yrs",
        data: [],
      },
      {
        name: "45-54 Yrs",
        data: [],
      },
      {
        name: "54-64 Yrs",
        data: [],
      },
      {
        name: "65+ Yrs",
        data: [],
      },
    ];
    /* Age && gender graph start */
    req.body.fields = "age,gender";

    var result = await getbrackdown(req.body);
    
   


    if (req.body.campaignId) {
      let a = result.data.filter(j=> {
        // var cost = j.campaign_id;
        return j.campaign_id == req.body.campaignId;
      });
      result.data =[...a]

    }
   
    let sortWeekdays = weekdays.sort();

    ageValue.forEach((obj, index) => {
      ageValue[index].data = sortWeekdays.map((obj) => 0);
    });

    genderValue.forEach((obj, index) => {
      genderValue[index].data = sortWeekdays.map((obj) => 0);
    });
    let ages = ["13-17", "18-24", "25-34", "35-44", "45-54", "55-64", "65+"];
    let gender = ["male", "female", "unknown"];

    for (let [xindex, x] of sortWeekdays.entries()) {
      for (let [yindex, y] of result.data.entries()) {
        if (
          req.body.dateFilter == "sixMonth" ||
          req.body.dateFilter == "thisYear"
        ) {
          if (y.date_start.includes(x) && y.age != "Unknown") {
            let findIndex = ages.indexOf(y.age);

            ageValue[findIndex].data[xindex] += parseInt(y.reach);
          }
          if (y.date_start.includes(x)) {
            let findIndex = gender.indexOf(y.gender);

            genderValue[findIndex].data[xindex] += parseInt(y.reach);
          }
        } else if (req.body.dateFilter == "allYear") {
          if (y.date_start.includes(x) && y.age != "Unknown") {
            let findIndex = ages.indexOf(y.age);

            ageValue[xindex].data[findIndex] += parseInt(y.reach);
          }
          if (y.date_start.includes(x)) {
            let findIndex = gender.indexOf(y.gender);

            genderValue[findIndex].data[xindex] += parseInt(y.reach);
          }
        } else {
          if (y.date_start == x && y.age != "Unknown") {
            let findIndex = ages.indexOf(y.age);

            ageValue[findIndex].data[xindex] += parseInt(y.reach);
          }
          if (y.date_start.includes(x)) {
            let findIndex = gender.indexOf(y.gender);

            genderValue[findIndex].data[xindex] += parseInt(y.reach);
          }
        }
      }
    }
    let genders = genderValue.map((obj) => {
      obj["data"] = obj.data.reduce((a, b) => a + b, 0);
      return obj;
    });
    if (!result)
      return res.status(500).send({ message: "Something went wrong" });

    return res
      .status(200)
      .send({ message: "Success", ageValue, genders, date: sortWeekdays });
  } catch (e) {
    return res.status(500).send({ message: "Something went wrong!", e });
  }
};
exports.analyticsreach = async (req, res) => {
 

  if (
    (!req.body.adAccount || !req.body.campaignId) &&
    !req.body.place_holder &&
    !req.body.userId &&
    (!req.body.dateFilter || !req.body.fromDate || !req.body.toDate)
  ) {
    return res.status(400).send({ message: "Check your input params" });
  }

  try {
    var userResponse = await User.findById(req.body.userId);

    if (!userResponse)
      return res.status(404).send({ message: "User Not found" });

    const businessResponse = await provider.findOne({
      userId: req.body.userId,
    });

    if (!businessResponse)
      return res.status(404).send({ message: " userID not found" });

    if (
      req.body.dateFilter == "thisMonth" ||
      req.body.dateFilter == "Last7Days" ||
      req.body.fromDate ||
      req.body.toDate
    ) {
      req.body.date = 2;
    } else {
      req.body.date = "monthly";
    }

    var weekdays = [];
    var weekdaysconverted = [];
    if (req.body.dateFilter == "Last7Days") {
      weekdays = [];
      for (var i = 0; i < 7; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "thisMonth") {
      weekdays = [];
      var sDate = moment().startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM-DD"));
        weekdaysconverted.push(tDate.format("MMM-DD"));
        tDate = moment(tDate).subtract(1, "days");
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "sixMonth") {
      var sDate = moment().subtract(5, "months").startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        tDate = moment(tDate).subtract(1, "month");
        weekdaysconverted.push(tDate.format("MMMM"));
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "thisYear") {
      weekdays = [];
      var sDate = moment().startOf("year");
      var eDate = moment().endOf("year");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        weekdaysconverted.push(tDate.format("MMMM"));
        tDate = moment(tDate).subtract(1, "month");
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "allYear") {
      weekdays = [];
      for (var i = 0; i < 3; i++) {
        weekdays.push(moment().subtract(i, "year").format("YYYY"));
        weekdaysconverted.push(moment().subtract(i, "year").format("YYYY"));
      }
      req.body.fromRange = weekdays[0].concat("-12-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01-01");
    } else if (req.body.fromDate && req.body.toDate) {
      req.body.toDate = moment(req.body.toDate).add(1, "days");
      weekdays = [];
      for (
        var m = moment(req.body.fromDate);
        m.isBefore(req.body.toDate);
        m.add(1, "days")
      ) {
        weekdays.push(m.format("YYYY-MM-DD"));
        weekdaysconverted.push(m.format("YYYY-MMM-DD"));
      }
      req.body.toRange = weekdays[0];
      req.body.fromRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Yesterday") {
      weekdays = [];
      weekdays.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Last30Days") {
      weekdays = [];
      for (var i = 0; i < 30; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Today") {
      weekdays = [];
      weekdays.push(moment().subtract(0, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    }
    //reach,improession, spend,conversions,cpc,clicks
    req.body.fields = "publisher_platform";
    var fbCampignDataAdSpent = await getdata(req.body);
    

    

    
    if (req.body.place_holder == "facebook") {
      var result = fbCampignDataAdSpent.data.filter(
        (element) => element.publisher_platform === "facebook"
      );
    } else if (req.body.place_holder == "instagram") {
      var result = fbCampignDataAdSpent.data.filter(
        (element) => element.publisher_platform === "instagram"
      );
    }

    // var fbCampignData = await getdata2(req.body);  
    // console.log("wfjfj",fbCampignData) 
       
    // var result = facebook.filter(function (o1) {
    //   return fbCampignData.data.some(function (o2) {
    //     return o1.id === o2.id;
    //   });
    // });

    // for (var ofIndex = 0; ofIndex < result?.length; ofIndex++) {
    //   fbCampignData.data?.forEach((item) => {
    //     if (result[ofIndex].id === item.id) {
    //       result[ofIndex].quality_ranking = item.quality_ranking;
    //     }
    //   });
    // }
    

    if (req.body.campaignId) {
     let a =  result.filter((j) => j.campaign_id== req.body.campaignId);
      result = [...a]
    }
  
    let sortWeekdays = weekdays.sort();
    let data = {
      reach: (reach = []),
      impressions: (impressions = []),
       spend: ( spend = []),
      clicks: (clicks = []),
      cpc: (cpc = []),
      conversions: (conversions = []),
      
    };
    

    for (let [xindex, x] of sortWeekdays.entries()) {
      reach[xindex] = 0;

      impressions[xindex] = 0;

       spend[xindex] = 0;

      clicks[xindex] = 0;

      cpc[xindex] = 0;

      conversions[xindex] = 0;

      for (let [yindex, y] of result.entries()) {
        if (
          req.body.dateFilter == "sixMonth" ||
          req.body.dateFilter == "thisYear"
        ) {
          if (y.date_start.includes(x) == true) {
            
            reach[xindex] = reach[xindex] + parseInt(y.reach);

            impressions[xindex] = impressions[xindex] + parseInt(y.impressions);

             spend[xindex] =   spend[xindex] + parseFloat(y. spend);

            clicks[xindex] = clicks[xindex] + parseInt(y.clicks);

            cpc[xindex] =  y.cpc  ? (cpc[xindex] + parseFloat(y.cpc)):cpc[xindex]     
            
            y.conversions?.map((j) => {
              var cost = j.value;

              conversions[xindex] = conversions[xindex] + parseInt(cost);
            });
          }
        } else if (req.body.dateFilter == "allYear") {
          if (y.date_start.includes(x)) {
            reach[xindex] = reach[xindex] + parseInt(y.reach);

            impressions[xindex] = impressions[xindex] + parseInt(y.impressions);

             spend[xindex] =  spend[xindex] + parseFloat(y. spend);

            clicks[xindex] = clicks[xindex] + parseInt(y.clicks);

            cpc[xindex] =  y.cpc  ? (cpc[xindex] + parseFloat(y.cpc)):cpc[xindex]
            y.conversions?.map((j) => {
              var cost = j.value;

              conversions[xindex] = conversions[xindex] + parseInt(cost);
            });
        
          }
        } else if (
          req.body.dateFilter == "thisMonth" ||
          req.body.dateFilter == "Last7Days"
        ) {
          if (y.date_start.includes(x) == true) {
            reach[xindex] = reach[xindex] + parseInt(y.reach);

            impressions[xindex] = impressions[xindex] + parseInt(y.impressions);

             spend[xindex] =  spend[xindex] + parseFloat(y. spend);

            clicks[xindex] = clicks[xindex] + parseInt(y.clicks);

            cpc[xindex] =  y.cpc  ? (cpc[xindex] + parseFloat(y.cpc)):cpc[xindex]


            y.conversions?.map((j) => {
              var cost = j.value;

              conversions[xindex] = conversions[xindex] + parseInt(cost);
            });
           
          }
        } else {
          if (y.date_start == x) {
            reach[xindex] = reach[xindex] + parseInt(y.reach);

            impressions[xindex] = impressions[xindex] + parseInt(y.impressions);

             spend[xindex] =  spend[xindex] + parseFloat(y. spend);

            clicks[xindex] = clicks[xindex] + parseInt(y.clicks);

            cpc[xindex] =  y.cpc  ? (cpc[xindex] + parseFloat(y.cpc)):cpc[xindex]

            y.conversions?.map((j) => {
              var cost = j.value;

              conversions[xindex] = conversions[xindex] + parseInt(cost);
            });
         
          }
        }
      }
    }



    if (!fbCampignDataAdSpent)
      return res.status(500).send({ message: "Something went wrong" });

    return res
      .status(200)
      .send({ message: "Success", data, date: sortWeekdays });
  } catch (e) {
    return res.status(500).send({ message: "Something went wrong!", e });
  }
};
exports.adAccount = async (req, res) => {
  if (!req.query.userId)
    return res.status(400).send({ message: "Check your input params" });
  try {
    var fbCampignDataAdSpent = await getadaccount(req.query);

    if (!fbCampignDataAdSpent)
      return res.status(500).send({ message: "Something went wrong" });

    return res.status(200).send({ message: "Success", fbCampignDataAdSpent });
  } catch (e) {
    return res.status(500).send({ message: "Something went wrong!", e });
  }
};
exports.allcampaign = async (req, res) => {
  if (!req.query.accountId && !req.query.userId)
    return res.status(400).send({ message: "Check your input params" });
  try {
    var fbCampignDataAdSpent = await getcampaign(req.query);

    if (!fbCampignDataAdSpent)
      return res.status(500).send({ message: "Something went wrong" });

    return res.status(200).send({ message: "Success", fbCampignDataAdSpent });
  } catch (e) {
    return res.status(500).send({ message: "Something went wrong!", e });
  }
};
exports.pageToken = async (req, res) => {
  if (!req.body.campaignId && !req.body.userId)
    return res.status(400).send({ message: "Check your input params" });
  try {
    const useeResponse = await User.findById(req.body.userId);
    if (!useeResponse)
      return res.status(200).send({ message: "User Id not Found" });

    var fbCampignDataAdSpent = await getpageid(
      req.body.campaignId,
      useeResponse._id
    );

    var data = fbCampignDataAdSpent.adsets.data[0].promoted_object.page_id;

    var pageidandaccesstoken = await pageToken(data, req.body);

    var followercount = await getpage(pageidandaccesstoken, req.body);

    if (!followercount)
      return res.status(500).send({ message: "Something went wrong" });

    return res.status(200).send({ message: "Success", followercount });
  } catch (e) {
    return res.status(500).send({ message: "Something went wrong!", e });
  }
};
exports.getUserName = async (req, res) => {
  if (!req.body.userId)
    return res.status(400).send({ message: "Check your input params" });

  const useeResponse = await User.findById(req.body.userId);

  if (!useeResponse)
    return res.status(200).send({ message: "User Id not Found" });

  const businessResponse = await provider.findOne({ userId: req.body.userId });
  if (!businessResponse)
    return res.status(500).send({message:"Business User not Found"})
  var fbUserId = businessResponse.response[0].userID;
  var fbAccessToken = businessResponse.response[0].accessToken;

  let fbuserDetails;
  if (fbUserId) {
    // getFbUserDetails
    fbuserDetails = await getFbUserDetails(fbUserId, fbAccessToken);
  }
  return res.status(200).send({
    message: "Facebook User details loaded successfully.",
    data: [fbuserDetails],
  });
};
// *****instagram *****///
exports.instagramMe = async (req, res) => {

  if (!req.body.userId && req.body.campaignId)
    return res.status(400).send({ message: "Check your input params" });
  try {
    const useeResponse = await User.findById(req.body.userId);

    if (!useeResponse)
      return res.status(200).send({ message: "User Id not Found" });

    var fbCampignDataAdSpent = await getpages(
      req.body.campaignId,
      useeResponse._id
    );

    var data = fbCampignDataAdSpent.adsets.data[0].promoted_object.page_id;

    var pageid = await instaBussinessaccount(data, useeResponse._id);

    var datas = pageid.instagram_business_account.id;

    var followercount = await follwercount(datas, req.body);

    if (!followercount)
      return res.status(500).send({ message: "Something went wrong" });

    return res.status(200).send({ message: "Success", followercount });
  } catch (e) {
    return res.status(500).send({ message: "Something went wrong!", e });
  }
};

exports.adrank = async (req, res) => {  
  if (
    (!req.body.adAccount || !req.body.campaignId) &&
    !req.body.place_holder &&
    !req.body.userId &&
    (!req.body.dateFilter || !req.body.fromDate || !req.body.toDate)
  ) {
    return res.status(400).send({ message: "Check your input params" });
  }

  try {
    var userResponse = await User.findById(req.body.userId);

    if (!userResponse)
      return res.status(404).send({ message: "User Not found" });

    const businessResponse = await provider.findOne({
      userId: req.body.userId,
    });

    if (!businessResponse)
      return res.status(404).send({ message: " userID not found" });

    if (
      req.body.dateFilter == "thisMonth" ||
      req.body.dateFilter == "Last7Days" ||
      req.body.fromDate ||
      req.body.toDate
    ) {
      req.body.date = 2;
    } else {
      req.body.date = "monthly";
    }

    var weekdays = [];
    var weekdaysconverted = [];
    if (req.body.dateFilter == "Last7Days") {
      weekdays = [];
      for (var i = 0; i < 7; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "thisMonth") {
      weekdays = [];
      var sDate = moment().startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM-DD"));
        weekdaysconverted.push(tDate.format("MMM-DD"));
        tDate = moment(tDate).subtract(1, "days");
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "sixMonth") {
      var sDate = moment().subtract(5, "months").startOf("month");
      var eDate = moment().endOf("month");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        tDate = moment(tDate).subtract(1, "month");
        weekdaysconverted.push(tDate.format("MMMM"));
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "thisYear") {
      weekdays = [];
      var sDate = moment().startOf("year");
      var eDate = moment().endOf("year");
      var tDate = eDate;
      while (tDate >= sDate) {
        weekdays.push(tDate.format("YYYY-MM"));
        weekdaysconverted.push(tDate.format("MMMM"));
        tDate = moment(tDate).subtract(1, "month");
      }
      req.body.fromRange = weekdays[0].concat("-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01");
    } else if (req.body.dateFilter == "allYear") {
      weekdays = [];
      for (var i = 0; i < 3; i++) {
        weekdays.push(moment().subtract(i, "year").format("YYYY"));
        weekdaysconverted.push(moment().subtract(i, "year").format("YYYY"));
      }
      req.body.fromRange = weekdays[0].concat("-12-31");
      req.body.toRange = weekdays[weekdays.length - 1].concat("-01-01");
    } else if (req.body.fromDate && req.body.toDate) {
      req.body.toDate = moment(req.body.toDate).add(1, "days");
      weekdays = [];
      for (
        var m = moment(req.body.fromDate);
        m.isBefore(req.body.toDate);
        m.add(1, "days")
      ) {
        weekdays.push(m.format("YYYY-MM-DD"));
        weekdaysconverted.push(m.format("YYYY-MMM-DD"));
      }
      req.body.toRange = weekdays[0];
      req.body.fromRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Yesterday") {
      weekdays = [];
      weekdays.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Last30Days") {
      weekdays = [];
      for (var i = 0; i < 30; i++) {
        weekdays.push(moment().subtract(i, "days").format("YYYY-MM-DD"));
        weekdaysconverted.push(moment().subtract(i, "days").format("dddd"));
      }
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    } else if (req.body.dateFilter == "Today") {
      weekdays = [];
      weekdays.push(moment().subtract(0, "days").format("YYYY-MM-DD"));
      weekdaysconverted.push(moment().subtract(1, "days").format("YYYY-MM-DD"));
      req.body.fromRange = weekdays[0];
      req.body.toRange = weekdays[weekdays.length - 1];
    }
        var fbCampignData = await getdata2(req.body); 
    
    var result = fbCampignData.data   
    if (req.body.campaignId) {
      let a =  result.filter((j) => j.campaign_id== req.body.campaignId);
      result = [...a]
     }        
     let sortWeekdays = weekdays.sort();
     let data = {Ad_rank: (maxdata = [])};
    
     let unknown = 0;
     let average = 0;
     let bello_avg = 0;
     value = [];
     var maxdata;
 
     for (let [xindex, x] of sortWeekdays.entries()) {          
 
       for (let [yindex, y] of result.entries()) {
                  if (req.body.dateFilter == "sixMonth" || req.body.dateFilter == "thisYear" ) {
           if (y.date_start.includes(x) == true) {
             if (y.quality_ranking == "UNKNOWN") {
               unknown += 1;
             } else if (y.quality_ranking == "AVERAGE") {
               average += 1;
             } else if (y.quality_ranking == "ABOVE_AVERAGE") {
               bello_avg += 1;
             } 
           }
         } else if (req.body.dateFilter == "allYear") {
           if (y.date_start.includes(x) == true){
            
             if (y.quality_ranking == "UNKNOWN") {
               unknown += 1;
             } else if (y.quality_ranking == "AVERAGE") {
               average += 1;
             } else if (y.quality_ranking == "ABOVE_AVERAGE") {
               bello_avg += 1;
             }
           }
         } else if (req.body.dateFilter == "thisMonth" ||req.body.dateFilter == "Last7Days") {
           if (y.date_start.includes(x) == true) {          
             if (y.quality_ranking == "UNKNOWN") {
               unknown += 1;
             } else if (y.quality_ranking == "AVERAGE") {
               average += 1;
             } else if (y.quality_ranking == "ABOVE_AVERAGE") {
               bello_avg += 1;
             }
           }
         } else {
           if  (y.date_start.includes(x) == true) {            
             if (y.quality_ranking == "UNKNOWN") {
               unknown += 1;
             } else if (y.quality_ranking == "AVERAGE") {
               average += 1;
             } else if (y.quality_ranking == "ABOVE_AVERAGE") {
               bello_avg += 1;
             }
           }
         }
       }
     }
 
     (unkown_count = unknown / 100), 
       (bello_count = bello_avg / 100),
       (average_count = average / 100);
 
     value.push(unkown_count, bello_count, average_count);  
 
     var highestdata = Math.max(...value);     
 
     maxdata.push(highestdata);
     if (!result)
       return res.status(500).send({ message: "Something went wrong" });
 
     return res
       .status(200)
       .send({ message: "Success", data, date: sortWeekdays });
   } catch (e) {
     return res.status(500).send({ message: "Something went wrong!", e });
   }
}
