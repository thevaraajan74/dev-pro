const User = require('../models/user.model');


exports.GetUser = async (req, res) => {
    const FindUser = await User.findById(req.params.id);

    if (!FindUser) return res.status(400).send({ status: 0, message: 'Invalid User' });

    return res.status(200).send({ status: 1, message: 'User Retrieved successfully.', data: FindUser });
}

exports.getall = async (req, res) => {
    const FindUser = await User.find();

    if (!FindUser) return res.status(400).send({ status: 0, message: 'Invalid User' });

    return res.status(200).send({ status: 1, message: 'User Retrieved successfully.', data: FindUser });

}


exports.UpdateUsers = async (req, res) => {

    const FindUser = await User.findOne({ id: req.body.id });

    if (!FindUser) return res.status(400).send({ status: 0, message: 'Invalid User' });

    const datas = (req.body)


    const UpdatedUser = await User.findByIdAndUpdate(FindUser._id, datas, { new: true });

    return res.status(200).send({ status: 1, message: 'User updated successfully.', data: UpdatedUser });
}


exports.DeleteUser = async (req, res) => {
    const DeleteUser = await User.removeUser(req.params.id);

    if (DeleteUser.ok === 0)
        return res.status(400).send({ status: 0, message: DeleteUser.error });

    return res.status(200).send({ status: 1, message: ' user Deleted Successfully' });
}
// exports.CreateUser = async (req, res) => {

//     //  if (!req.body.first_name && !req.body.last_name &&req.body.email && req.body.password   ) return res.status(400).send({ message: 'Check your input params' })

//     const Createban = new User(req.body);
//     Createban.save()
//         .then((data) => {
//             if (!data) return res.status(400).send({ status: 0, message: 'Server Error' });
//             return res.status(200).send({ status: 1, message: ' User Created Successfully', data });
//         }).catch(error => {
//             return res.status(400).send({ status: 0, message: error.code === 11000 ? Object.keys(error.keyValue)[0] + ' is already exists' : error.message });
//         })
// };


