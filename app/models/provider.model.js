const mongoose = require('mongoose');
const User = require('./user.model')


// Setup schema
var ProviderSchema = mongoose.Schema({

    provider: { type: String, required: [true, "provider cannot be blank"], enum: ['facebook', 'twitter', 'instagram', 'apple'] },

    userId: { type: mongoose.Schema.Types.ObjectId, ref: User },

    response: { type: Array, required: [true, "response cannot be blank"] },

    is_active: { type: Boolean, default: true, enum: [true, false] },

    is_deleted: { type: Boolean, default: false, enum: [true, false] },

}, { timestamps: true });

// Export provider model
var providers = module.exports = mongoose.model('provider', ProviderSchema);