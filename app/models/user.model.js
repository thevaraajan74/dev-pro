const mongoose = require('mongoose');
const Validate = require('../library/regex.validator');

// Setup schema
var userSchema = mongoose.Schema({    

    first_name: { type: String, required: [true, "Firstname cannot be blank"], default: null },

    last_name: { type: String, required: [true, "Lastname cannot be blank"],  default: null },

    email: { type: String, required: [true, "Email cannot be blank"], unique: true, lowercase: true, validate: [Validate.EmailValidate, 'Please enter a valid email address'], },

    password: { type: String, default: null },

    is_verified: { type: Boolean, default: false, enum: [true, false] },

    is_active: { type: Boolean, default: false, enum: [true, false] },

    is_deleted: { type: Boolean, default: false, enum: [true, false] },

    token: { type: String, default: null }

}, { timestamps: true });

// Export user model
var Users = module.exports = mongoose.model('user', userSchema);