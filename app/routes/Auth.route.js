module.exports = (app) => {
    const User = require('../../app/controller/Auth.controller');

    app.post("/login", User.LoginUser);        // User Login
    app.post("/logout", User.LogoutUser)   // User logout
    app.post("/changepasssword", User.changepasssword) // User changepasssword
    app.post("/verifyToken", User.Tokenverfiy) // verify token
    
};
