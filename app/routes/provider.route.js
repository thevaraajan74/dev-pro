module.exports = (app) => {

    const provider = require('../../app/controller/provider.controller');
    app.get("/provider", provider.providerDetails);      // Get providerUser
    app.get("/providers", provider.getallprovider)       // get all provider
    app.put("/provider/:userId", provider.Updateprovider);       // Update provider
    app.delete('/provider', provider.Deleteprovider);  //delete provider
    app.post("/provider", provider.Createprovider) //create new provider

}