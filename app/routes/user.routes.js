module.exports = (app) => {
  const User = require('../../app/controller/user.controller');

  app.get("/user/:id", User.GetUser);      // Get Single User
  app.put("/users", User.UpdateUsers);       // Update User
  app.delete('/user/:id', User.DeleteUser);  //delete user
  //app.post("/user", User.CreateUser) //create new user
  app.get('/userall', User.getall) // get all users

}
