const { compare } = require('bcrypt');
var jwt = require('jsonwebtoken');
require('dotenv').config();
const User = require('../models/user.model')

exports.UserVerify = async (req, res,next) => {

  var Token = req.headers['x-access-token']
  
  if (Token) {
      // verify secret and checks exp
      try {
          let id = jwt.verify(Token, process.env.PRIVATE_KEY);           

          const verify = await User.findById(id._id );           
         
           if (Token == verify.token){

            return next()
           }  
           return res.send({message:" Invalid Token"})          

      } catch (err) {

          return res.status(401).send({ status: 0, message: 'Invalid Access Token' });

      }
  }
  else return res.status(401).send({ status: 0, message: "Invalid Access Token" });
};

