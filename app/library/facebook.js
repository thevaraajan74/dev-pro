const RequestPromise = require("request-promise");
const provider = require("../models/provider.model");
//
exports.getadaccount = async (req) => {
  var datas = await provider.findOne({ userId: req.userId });

  var data = {
    uri: `https://graph.facebook.com/v13.0/${datas.response[0].userID}?access_token=${datas.response[0].accessToken}&fields=adaccounts{name}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getdata = async (req) => {
  var datas = await provider.findOne({ userId: req.userId });
  var data = {
    uri: `https://graph.facebook.com/v13.0/${req.adAccount}/insights?fields=campaign_id,campaign_name,impressions,spend,reach,cpc,clicks,quality_ranking,conversions&breakdowns=${req.fields}&time_range[since]=${req.toRange}&time_range[until]=${req.fromRange}&time_increment=${req.date}&level=ad&limit=1000&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getdata2 = async (req) => {
  var datas = await provider.findOne({ userId: req.userId });
  var data = {
    uri: `https://graph.facebook.com/v13.0/${req.adAccount}/insights?fields=campaign_id,campaign_name,quality_ranking&time_range[since]=${req.toRange}&time_range[until]=${req.fromRange}&time_increment=${req.date}&level=ad&limit=1000&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getbrackdown = async (req) => {
  var datas = await provider.findOne({ userId: req.userId });
  var data = {
    uri: `https://graph.facebook.com/v13.0/${req.adAccount}/insights?fields=campaign_id,campaign_name,impressions,spend,reach,cost_per_thruplay,clicks,quality_ranking,conversions&breakdowns=${req.fields}&time_range[since]=${req.toRange}&time_range[until]=${req.fromRange}&time_increment=${req.date}&level=ad&limit=1000&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };

  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getcampaign = async (req) => {
  var datas = await provider.findOne({ userId: req.userId });

  var getStatus = {
    uri: `https://graph.facebook.com/v13.0/${req.accountId}/campaigns?fields=name&access_token=${datas.response[0].accessToken}`,
    method: "get",
  };
  let facebook = await RequestPromise(getStatus);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getpage = async (req, date) => {
  var data = {
    uri: `https://graph.facebook.com/v13.0/${req.id}?fields=followers_count&access_token=${req.access_token}`,

    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getpageid = async (req, res) => {
  var datas = await provider.findOne({ userId: res });

  var data = {
    uri: `https://graph.facebook.com/v12.0/${req}?fields=name,adsets{promoted_object}&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};

exports.pageToken = async (req, res) => {
  var datas = await provider.findOne({ userId: res.userId });

  var data = {
    uri: `https://graph.facebook.com/v12.0/${req}?fields=access_token&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getFbUserDetails = async (fbUserId, fbAccessToken) => {
  var data = {
    uri: `https://graph.facebook.com/v12.0/${fbUserId}?fields=id,name&access_token=${fbAccessToken}&&__cppo=1&format=json&method=get&pretty=0&suppress_http_code=1&transport=cors`,

    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.getlonglive = async (req) => { 
 
  var data = {
    url: `https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${process.env.CILENT_ID}&client_secret=${process.env.CLIENT_SECRET}&fb_exchange_token=${req.response[0].accessToken}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data); 
  facebook = JSON.parse(facebook);
  if (facebook) {    
    return facebook.access_token;
  }
};
