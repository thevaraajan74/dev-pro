const User = require("../../app/models/user.model");
const bcrypt = require("bcrypt");

exports.SeedUser = async () => {
  const users = [
    {
      first_name: "Govind",
      last_name: "M",
      email: "govind@gmail.com",
      password: bcrypt.hashSync("govind@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "Kavi",
      last_name: "Arasan",
      email: "kavi@yopmail.com",
      password: bcrypt.hashSync("kavi@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "Santhosh",
      last_name: "Mani",
      email: "santhosh@yopmail.com",
      password: bcrypt.hashSync("san@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "admin",
      last_name: "Mani",
      email: "admin@yopmail.com",
      password: bcrypt.hashSync("admin@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "admin1",
      last_name: "Mani",
      email: "admin1@yopmail.com",
      password: bcrypt.hashSync("admin1@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "Silvanus",
      last_name: "Marie",
      email: "silvanus@yopmail.com",
      password: bcrypt.hashSync("Renae123$", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "John",
      last_name: "Smith",
      email: "johnsmith@yopmail.com",
      password: bcrypt.hashSync("john@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
    {
      first_name: "Vel",
      last_name: "Tester",
      email: "vel@yopmail.com",
      password: bcrypt.hashSync("vel@123", 10),
      is_active: true,
      is_delete: false,
      is_verified: true,
    },
  ];

  for (const u of users) {
    const user = new User(u);
    await user.save();
  }

  return;
};
