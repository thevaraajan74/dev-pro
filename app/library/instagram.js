const RequestPromise = require("request-promise");
const provider = require("../models/provider.model");

exports.follwercount = async (req, res) => {
  var datas = await provider.findOne({ userId: res.userId });
  var data = {
    uri: `https://graph.facebook.com/v13.0/${req}?fields=id,name,followers_count&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };
  let instagram = await RequestPromise(data);
  instagram = JSON.parse(instagram);
  if (instagram) {
    return instagram;
  }
};
exports.getpages = async (req, res) => {
  var datas = await provider.findOne({ userId: res });
  var data = {
    uri: `https://graph.facebook.com/v12.0/${req}?fields=name,adsets{promoted_object}&access_token=${datas.response[0].accessToken}`,
    method: "GET",
  };
  let facebook = await RequestPromise(data);
  facebook = JSON.parse(facebook);
  if (facebook) {
    return facebook;
  }
};
exports.instaBussinessaccount = async (req, res) => {
  var datas = await provider.findOne({ userId: res });
  var data = {
    uri: `https://graph.facebook.com/v13.0/${req}?fields=instagram_business_account&access_token=${datas.response[0].accessToken}`,
  };
  let instagram = await RequestPromise(data);
  instagram = JSON.parse(instagram);
  if (instagram) {
    return instagram;
  }
};
