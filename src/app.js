const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
dotenv.config();
const app = express();

const SeedUser = require('../app/library/seed');
const Auth = require('../app/middleware/verifyToken');
app.use(cors());

//Init Express
// require('./lib/upload')

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true, limit: '50mb' }));

// parse requests of content-type - application/json
app.use(express.json({ limit: '50mb' }));

// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     if (req.method === "OPTIONS") {
//         return res.status(200).end();
//     }
//     next();
// });

//mongodb connected
mongoose.set('runValidators', true);
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE_NAME, { useUnifiedTopology: true, useNewUrlParser: true });
var db = mongoose.connection
//SeedUser.SeedUser();
if (!db)
    console.log("Error connecting db");
else
    console.log("Db connected successfully");


app.use(cors())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

//Router
var apiRoutesAuthUser = express.Router();
 apiRoutesAuthUser.use(Auth.UserVerify);
require('../app/routes/user.routes')(apiRoutesAuthUser);
require('../app/routes/provider.route')(apiRoutesAuthUser);
require('../app/routes/analastic.route')(apiRoutesAuthUser);
app.use('/api/v1/', apiRoutesAuthUser)

var apiRoutesAuth = express.Router();
require('../app/routes/Auth.route')(apiRoutesAuth);
app.use('/api',apiRoutesAuth)

app.use(express.static(path.join(__dirname, 'public')))
app.use("/", express.static(path.join(__dirname, "public/customer")));


module.exports = app;